@author William Tran

line 2: sets the stack pointer to an empty spot in memory in order to store parameters
line 3, 4: stores parameters in memory for when we need to use them in the function
line 5, 6: loads the previous parameters from memory into registers so we can use them in calculations
line 7: computes the function
line 8: restores the stack pointer to the previous place before this function
line 9: branches the instructions to the next function to continue the program

line 11: clears the register if it is full so we have space for the function
line 12, 13: moves the stack pointer to an empty space in memory, enough to store the parameters
line 14: stores the parameter in memory for when we need to use it in the function
line 15: loads the previous parameters from memory into registers so we can use them in calculations
line 16: tells the machine what type of data the register is storing
lien 17: calls printf to print the parameter
line 18: restores the stack pointer to the previous place before this function
line 19: restores the register data to before this function if it was cleared
line 20: branches the instructions to the next function to continue the program



![chrome_dGGY2vQz3U.png](chrome_dGGY2vQz3U.png)
